# Changelog

## 0.2.0 - 2020-06-26

### Added

- Plaftorms in SVG format
- Backgrounds in SVG format
- Example webpages for platforms, backgrounds and items
- A changelog

## 0.1.0 - 2020-06-10

### Added

- Base game items in SVG format
- Base game sub-items in SVG format
