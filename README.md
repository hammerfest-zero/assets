# Hammerfest assets

This repository is a collection of assets available in the Hammerfest game. To use them in your project, just add this repository as a git module. You can also use symlinks to make it compliant with your project architecture while remaining easy to update.  
All the files are using SVG format, embedding raster images (png or jpeg ones) when necessary. This makes it easier to use (you don't have to remember which asset is using which extension) and more future-proof (some assets might be rewritten in true svg).

# Images
## Items

The path of an item is `/images/items/:id:.svg`. For items with several versions (like the crystals or the letters), the subitems can be found in a subdirectory, with paths having this form `/images/items/:id:/:subid:.svg`. The original item is still available with the default path.  
Note that subitems ids start from 0.

### Examples

- `/images/items/1237.svg` -> ![Red bomb image](images/items/1237.svg)
- `/images/items/0.svg` -> ![Letters](images/items/0.svg)
- `/images/items/0/3.svg` -> ![S Letter](images/items/0/3.svg)

## Platforms

Each platform is split in two parts, a body (the main part of the platform) and an end (the last few pixels of the platform). These two parts are located in `/images/platforms/bodies/:id:.svg` and `/images/platforms/ends/:id:.svg`.  
To display a small platform, you have to use the `N * 20 - 5` first columns of the body (where N is the length of the platform in block size) and then append the end.  
Note that platforms ids start from 1.

### Examples

- `/images/platforms/bodies/1.svg` `/images/platforms/ends/1.svg` -> ![Raster platform body](images/platforms/bodies/1.svg)![Raster platform end](images/platforms/ends/1.svg)
- `/images/platforms/bodies/41.svg` `/images/platforms/ends/41.svg` -> ![Vector platform body](images/platforms/bodies/41.svg)![Vector platform end](images/platforms/ends/41.svg)
```html
<!-- An example dispaying a 8 blocks long platform using HTML/CSS with in-game like shadows
     More examples can be found in the examples/ directory -->
<style>
  div > :nth-child(1) { box-shadow: 3px 4px rgba(0,0,0,0.3); object-fit: cover; object-position: left; height: 20px; }
  div > :nth-child(2) { box-shadow: 3px 4px rgba(0,0,0,0.3); }
</style>
<div>
  <img src="./images/platforms/bodies/1.svg" style="width:155px" />
  <img src="./images/platforms/ends/1.svg" />
</div>
```

## Backgrounds

The path of a background is `/images/backgrounds/:id:.svg`.  
Note that backgrounds ids start from 1.

### Examples

- `/images/backgrounds/1.svg` -> ![base background](images/backgrounds/1.svg)
